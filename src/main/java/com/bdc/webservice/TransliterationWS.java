package com.bdc.webservice;

import com.bdc.webservice.dto.TransliterationRequest;
import com.bdc.webservice.dto.TransliterationResponse;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.io.IOException;


@WebService(name = "TransliterationWS")
public interface TransliterationWS {

    // balance inquiry
    @WebResult(name = "response")
    TransliterationResponse getTranslations(@WebParam TransliterationRequest request) throws IOException;

}
