package com.bdc.webservice;

import com.basistech.rosette.api.HttpRosetteAPI;
import com.basistech.rosette.apimodel.CommonRosetteAPIException;
import com.basistech.rosette.apimodel.NameTranslationRequest;
import com.basistech.rosette.apimodel.NameTranslationResponse;
import com.bdc.webservice.dto.TranslationBean;
import com.bdc.webservice.dto.TransliterationRequest;
import com.bdc.webservice.dto.TransliterationResponse;

import javax.jws.WebService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebService(endpointInterface = "com.bdc.webservice.TransliterationWS" )
public class TransliterationWSimpl implements TransliterationWS {


    Logger logger = Logger.getLogger(TransliterationWSimpl.class.getName());

    private static final String KEY_PROP_NAME = "70761CE3D2BCD3E9AE0C6075B0BFB8965273E249B26AED683825287684141B95B97FB242A99C703552516D5E8E98360056CDAA25A4256D327F5308686946959EC0730E2E60106862F7B0F1EFF668DCBAA46ACAD596618C1C";//"rosette.api.key";
    private static final String URL_PROP_NAME = "http://172.17.63.152:8181/rest/v1/";//"rosette.api.altUrl";

    @Override
    public TransliterationResponse getTranslations(TransliterationRequest request) throws IOException, CommonRosetteAPIException {

        TransliterationResponse translation_repsonse = new TransliterationResponse();
        List<TranslationBean> translations = new ArrayList<TranslationBean>();
        try {

            NameTranslationRequest request1 = NameTranslationRequest.builder()
                    .name(request.getArabicName())
                    //.targetLanguage(LanguageCode.ENGLISH)
                    .build();

            HttpRosetteAPI rosetteApi = new HttpRosetteAPI.Builder()
                    .key(getApiKeyFromSystemProperty())
                    .url(getAltUrlFromSystemProperty())
                    .build();
            //The api object creates an http client, but to provide your own:
            //api.httpClient(CloseableHttpClient)
            NameTranslationResponse response;

            response = rosetteApi.perform(HttpRosetteAPI.NAME_TRANSLATION_SERVICE_PATH, request1, NameTranslationResponse.class);


            TranslationBean t = new TranslationBean();
            t.setName(request.getArabicName());
            t.setNameTranslation(response.getTranslation());
            t.setConfidence(response.getConfidence());
            translations.add(t);
            translation_repsonse.setTranslations(translations);
        } catch (Exception e) {
            translation_repsonse.setErrCod("200");
            translation_repsonse.setErrMsg("Transilteration server internal Error");
            e.printStackTrace();
            logger.log(Level.WARNING, e.getMessage());
        }

        return translation_repsonse;
    }

    /**
     *
     */
    protected String getApiKeyFromSystemProperty() {
        String apiKeyStr = KEY_PROP_NAME;//System.getProperty(KEY_PROP_NAME);
        if (apiKeyStr == null || apiKeyStr.trim().length() < 1) {
            //showUsage(getClass());
            System.exit(1);
        }
        return apiKeyStr.trim();
    }

    /**
     *
     */
    protected String getAltUrlFromSystemProperty() {
        String altUrlStr = URL_PROP_NAME;//System.getProperty(URL_PROP_NAME);
        if (altUrlStr == null || altUrlStr.trim().length() < 1) {
            altUrlStr = "https://api.rosette.com/rest/v1";
        }
        return altUrlStr.trim();
    }


}

