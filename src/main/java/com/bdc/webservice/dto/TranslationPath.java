package com.bdc.webservice.dto;


public class TranslationPath
{
  private String langSource;
  
  private String langTarget;
  
  private String schemeSource;
  
  private String schemeTarget;
  
  private String scriptSource;
  
  private String scriptTarget;
  
  private String status;
  
  private String transName;
  

  public TranslationPath() {}
  
  public TranslationPath(String transName, String scriptSource, String langSource, String schemeSource, String scriptTarget, String langTarget, String schemeTarget)
  {
    this.transName = transName;
    this.scriptSource = scriptSource;
    this.langSource = langSource;
    this.schemeSource = schemeSource;
    
    this.scriptTarget = scriptTarget;
    this.langTarget = langTarget;
    this.schemeTarget = schemeTarget;
  }
  


  public String getLangSource()
  {
    return langSource;
  }
  
  public void setLangSource(String langSource) {
    this.langSource = langSource;
  }
  
  public String getLangTarget() {
    return langTarget;
  }
  
  public void setLangTarget(String langTarget) {
    this.langTarget = langTarget;
  }
  
  public String getSchemeSource() {
    return schemeSource;
  }
  
  public void setSchemeSource(String schemeSource) {
    this.schemeSource = schemeSource;
  }
  
  public String getSchemeTarget() {
    return schemeTarget;
  }
  
  public void setSchemeTarget(String schemeTarget) {
    this.schemeTarget = schemeTarget;
  }
  
  public String getScriptSource() {
    return scriptSource;
  }
  
  public void setScriptSource(String scriptSource) {
    this.scriptSource = scriptSource;
  }
  
  public String getScriptTarget() {
    return scriptTarget;
  }
  
  public void setScriptTarget(String scriptTarget) {
    this.scriptTarget = scriptTarget;
  }
  
  public String getStatus() {
    return status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getTransName() {
    return transName;
  }
  
  public void setTransName(String transName) {
    this.transName = transName;
  }
}