package com.bdc.webservice.dto;


public class TranslationBean
{
  private String name;
  private String nameTranslation;
  private Double confidence;
  private Boolean selected = Boolean.valueOf(false);
  

  public TranslationBean() {}
  

  public String getName()
  {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getNameTranslation() {
    return nameTranslation;
  }
  
  public void setNameTranslation(String nameTranslation) {
    this.nameTranslation = nameTranslation;
  }
  
  public Double getConfidence() {
    return confidence;
  }
  
  public void setConfidence(Double confidence) {
    this.confidence = confidence;
  }
  
  public Boolean getSelected() {
    return selected;
  }
  
  public void setSelected(Boolean selected) {
    this.selected = selected;
  }
}