package com.bdc.webservice.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "TransliterationResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransliterationResponse {
    private String errCod;
    private String errMsg;
    private List<TranslationBean> translations;


    public TransliterationResponse() {
        this.setTranslations(null);
        this.errCod = "0";
        this.errMsg = " ";

    }

    public String getErrCod() {
        return errCod;
    }

    public void setErrCod(String errCod) {
        this.errCod = errCod;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public List<TranslationBean> getTranslations() {
        return translations;
    }

    public void setTranslations(List<TranslationBean> translations) {
        this.translations = translations;
    }

}
