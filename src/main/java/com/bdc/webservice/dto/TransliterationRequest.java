package com.bdc.webservice.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "TransliterationRequest")
@XmlAccessorType(XmlAccessType.FIELD)

public class TransliterationRequest {
    //@XmlElement(required=true,nillable=false)
    @NotNull
    private String arabicName;

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }


}
